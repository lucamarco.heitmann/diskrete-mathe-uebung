package de.lhe.diskretemathe;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Uebungsblatt4Aufgabe4 {
    public static void main(String[] args) {
        try (var scanner = new Scanner(System.in)) {
            //noinspection InfiniteLoopStatement
            while (true) {
                try {
                    System.out.print("n = ");
                    var n = scanner.nextInt();
                    scanner.nextLine();
                    System.out.printf("eratosthenes(%d) = %s%n", n, Arrays.toString(eratosthenes(n)));
                } catch (IllegalArgumentException e) {
                    System.out.println("Illegal input: " + e.getMessage());
                }
            }
        }
    }

    private static int[] eratosthenes(int n) {
        var result = IntStream.range(1, n);
        for (int i = 2; i <= n; i++) {
            int finalI = i;
            result = result.filter(x -> x == finalI || x % finalI != 0);
        }
        return result.toArray();
    }
}
