package de.lhe.diskretemathe;

import java.util.Scanner;

public class Uebungsblatt3Aufgabe4 {
    public static void main(String[] args) {
        try (var scanner = new Scanner(System.in)) {
            //noinspection InfiniteLoopStatement
            while (true) {
                try {
                    System.out.print("a = ");
                    var a = scanner.nextInt();
                    System.out.print("b = ");
                    var b = scanner.nextInt();
                    System.out.printf("ggT(%d, %d) = %d%n", a, b, ggT(a, b));
                } catch (IllegalArgumentException e) {
                    System.out.println("Illegal input: " + e.getMessage());
                }
            }
        }
    }

    private static int ggT(int a, int b) {
        if (a > 0) {
            var r = b % a;
            while (r != 0) {
                b = a;
                a = r;
                r = b % a;
            }
            return a;
        } else {
            throw new IllegalArgumentException("a needs to be > 0");
        }
    }
}
