package de.lhe.diskretemathe;

import java.util.Scanner;

public class Uebungsblatt3Aufgabe5 {
    public static void main(String[] args) {
        try (var scanner = new Scanner(System.in)) {
            //noinspection InfiniteLoopStatement
            while (true) {
                try {
                    System.out.print("input = ");
                    var input = scanner.nextLine();
                    System.out.print("k = ");
                    var k = scanner.nextInt();
                    scanner.nextLine();
                    System.out.printf("caesar(%s, %d) = %s%n", input, k, caesar(input, k));
                } catch (IllegalArgumentException e) {
                    System.out.println("Illegal input: " + e.getMessage());
                }
            }
        }
    }

    private static String caesar(String input, int k) {
        if (k < 1 || k > 26) {
            throw new IllegalArgumentException("k needs to be >= 1 and <= 26");
        } else if (input.chars().anyMatch(m -> (m < 'a' || m > 'z') && m != ' ')) {
            throw new IllegalArgumentException("input text must only contain lowercase characters or a space");
        } else {
            return input.chars()
                    .map(m -> m == ' ' ? ' ' : (m - 'a' + k) % 26 + 'a')
                    .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                    .toString();
        }
    }
}
