package de.lhe.diskretemathe;

import java.util.Scanner;

public class Uebungsblatt5Aufgabe5 {

    public static void main(String[] args) {
        try (var scanner = new Scanner(System.in)) {
            //noinspection InfiniteLoopStatement
            while (true) {
                try {
                    System.out.print("n = ");
                    var n = scanner.nextInt();
                    scanner.nextLine();
                    System.out.printf("fib(%d) = %,d%n", n, fib(n));
                } catch (IllegalArgumentException e) {
                    System.out.println("Illegal input: " + e.getMessage());
                }
            }
        }
    }

    private static long fib(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("n cannot be less than 0");
        } else if (n <= 2) {
            return 1;
        } else {
            return fib(n - 2) + fib(n - 1);
        }
    }
}
