package de.lhe.diskretemathe;

import java.util.Scanner;

public class Uebungsblatt5Aufgabe4 {
    public static void main(String[] args) {
        try (var scanner = new Scanner(System.in)) {
            //noinspection InfiniteLoopStatement
            while (true) {
                try {
                    System.out.print("palindrom = ");
                    var palindrom = scanner.nextLine();
                    System.out.printf("checkPalindrom(\"%s\") = %s%n", palindrom, checkPalindrom(palindrom));
                } catch (IllegalArgumentException e) {
                    System.out.println("Illegal input: " + e.getMessage());
                }
            }
        }
    }

    private static boolean checkPalindrom(String palindrom) {
        if (palindrom == null || palindrom.isBlank()) {
            throw new IllegalArgumentException("palindrom must not be blank");
        } else {
            for (int i = 0; i < palindrom.length() / 2; i++) {
                if (palindrom.charAt(i) != palindrom.charAt(palindrom.length() - i - 1)) {
                    return false;
                }
            }
            return true;
        }
    }
}
